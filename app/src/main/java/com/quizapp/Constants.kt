package com.quizapp

import com.quizapp.R
import com.quizapp.Question

object Constants {
    const val USER_NAME: String = "user_name"
    const val TOTAL_QUESTIONS: String = "total_questions"
    const val CORRECT_ANSWERS: String = "correct_answers"

    fun getQuestions(): ArrayList<Question> {
        val questionsList = ArrayList<Question>()

        // 1
        val que1 = Question(
            1, "Hukum bacaan apa yang diberi tanda berwarna merah berikut ini??",
            R.drawable.soalquiz1,
            "Ikhfa Haqiqi", "Mad",
            "Idzhar Halqi", "Idgham Bigunnah", 1
        )

        questionsList.add(que1)

        // 2
        val que2 = Question(
            2, "Hukum bacaan apa yang diberi tanda berwarna merah berikut ini?",
            R.drawable.soalquiz2,
            "Idzhar Halqi", "Mad",
            "Ikhfa Haqiqi", "Idgham Bigunnah", 3
        )

        questionsList.add(que2)

        // 3
        val que3 = Question(
            3, "Hukum bacaan apa yang diberi tanda berwarna merah berikut ini?",
            R.drawable.soalquiz3,
            "Mad Jaiz Munfashil", "Alif Lam Syamsiyah",
            "Alif Lam Qomariah", "Mad Wajib Muttashil", 3
        )

        questionsList.add(que3)

        // 4
        val que4 = Question(
            4, "Hukum bacaan apa yang diberi tanda berwarna merah berikut ini?",
            R.drawable.soalquiz4,
            "Mad Wajib Muttashil", "Mad Jaiz Munfashil",
            "Mad Iwadh", "Mad Thabi'i", 2
        )

        questionsList.add(que4)

        // 5
        val que5 = Question(
            5, "Hukum bacaan apa yang diberi tanda berwarna merah berikut ini?",
            R.drawable.soalquiz5,
            "Idzhar Syafawi", "Idgham Mimi",
            "Ikhfa Syafawi", "Ikhfa Haqiqi", 3
        )

        questionsList.add(que5)

        // 6
        val que6 = Question(
            6, "Hukum bacaan apa yang diberi tanda berwarna merah berikut ini?",
            R.drawable.soalquiz6,
            "Idzhar Syafawi", "Ikhfa Syafawi",
            "Ikhfa Haqiqi", "Idgham Mimi", 2
        )

        questionsList.add(que6)

        // 7
        val que7 = Question(
            7, "Terjemah yang benar untuk kalimat yang digarisbawahi pada ayat di atas adalah?",
            R.drawable.soalquiz7,
            "Dan janganlah kamu mendekati zina", "Dan janganlah kamu melakukan zina",
            "Dan suatu jalan yang buruk", "(Zina) itu suatu jalan yang tidak diridhai", 3
        )

        questionsList.add(que7)

        // 8
        val que8 = Question(
            8, "Hukum bacaan untuk penggalan  yang bergaris bawah adalah?",
            R.drawable.soalquiz8,
            "Mad wajib munfasil, gunnah, izhar", "Mad badal, gunnah, idgom",
            "Mad ashli, gunnah, iqlab", "Mad ashli, gunnah, izhar", 4
        )

        questionsList.add(que8)

        // 9
        val que9 = Question(
            9, "Cara membacanya?",
            R.drawable.soalquiz9,
            "Samar-samar", "Jelas",
            "Mendengung", "Dipantulkan", 3
        )

        questionsList.add(que9)

        // 10
        val que10 = Question(
            10, "Hukum bacaan apa yang pada soal berikut ini?",
            R.drawable.soalquiz10,
            "Al-qomariyah", "Al-Syamsyiah",
            "Alif Lam", "Alif Lam Bighunnah", 1
        )

        questionsList.add(que10)

        return questionsList
    }
}